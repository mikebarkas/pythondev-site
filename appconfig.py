import os
import logging

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:

    @staticmethod
    def init_app(app):
        pass


class Production(Config):
    LOGGING_LEVEL = logging.WARNING


class Debug(Config):
    TESTING = True
    DEBUG = True
    LOGGING_LOCATION = 'pythondev.log'
    LOGGING_LEVEL = logging.DEBUG


class Test(Config):
    TESTING = True
    LOGGING_LEVEL = logging.DEBUG


config_dict = {'default': Debug,
               'production': Production,
               'testing': Test,
               'debug': Debug,
               }
