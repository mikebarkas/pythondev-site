from . import main

from flask import render_template


@main.route('/')
@main.route('/index')
def index():
    return render_template('main/index.html')


@main.route('/projects')
def projects():
    return render_template('main/projects.html')


@main.route('/blog')
def blog():
    return render_template('main/blog.html')
